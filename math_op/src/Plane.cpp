#include "math_op/Transform.hpp"
#include "math_op/Plane.hpp"

namespace MathOp
{

int Plane::setParams(double a, double b, double c, double d)
{
	double norm = sqrt(a*a+b*b+c*c);
	a_ = a/norm;
	b_ = b/norm;
	c_ = c/norm;
	d_ = d/norm;

	return 0;
}

Plane Plane::transform(const MathOp::Transform& trans) const
{
	cv::Vec3d norm = getNormal();
	cv::Point3d point = getPointInPlane();

	norm = trans * norm;
	norm = cv::normalize(norm);
	point = trans * point;

	Plane plane_t;
	plane_t.setParams(norm[0], norm[1], norm[2],
					  -1.0 * (norm[0]*point.x + norm[1]*point.y + norm[2]*point.z) );

	return plane_t;
}

double Plane::distanceTo(const cv::Point3d& p) const
{
	double D = (a_*p.x + b_*p.y + c_*p.z) + d_;

	return D;
}

cv::Point3d Plane::getPointInPlane() const
{
	//     ax+by+cz+d=0
	// <=> z = -(ax+by+d)/c
	// <=> y = -(ax+cz+d)/b
	// <=> x = -(by+cz+d)/a
	cv::Point3d p;
	if (c_ != 0.0)
	{
		p.x = 0.0;
		p.y = 0.0;
		p.z = -1 * d_/c_;
	}
	else if (b_ != 0.0)
	{
		p.x = 0.0;
		p.y = -1 * d_/b_;
		p.z = 0.0;
	}
	else if (b_ != 0.0)
	{
		p.x = -1 * d_/a_;
		p.y = 0.0;
		p.z = 0.0;
	}
	else
	{
		std::cerr << "Plane normal is zero - can't find point in plane." << std::endl;
	}
}

}
